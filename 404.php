<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <header class="header-empty">
                <div class="container">
                    <a class="header-logo" href="#">
                        <img src="img/header_logo.png" class="img-responsive" alt="">
                    </a>
                </div>
            </header>
            <!-- -->

            <section class="main">
                <div class="container">
                    <div class="error-content">
                        <strong>404</strong>
                        <span>К сожалению мы не смогли найти эту страницу</span>
                    </div>
                    <div class="error-text">Вы можете перейти в популярные<br/>разделы сайта:</div>
                    <ul class="error-link">
                        <li><a href="#" class="btn btn-border">женское</a></li>
                        <li><a href="#" class="btn btn-border">мужское</a></li>
                    </ul>
                </div>
            </section>

        </div>

        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>