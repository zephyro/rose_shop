
$(function() {
    var pull = $('.navbar-toggle');
    var menu = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.toggleClass('open');
    });

    $(window).resize(function(){
        var w = $(window).width();
        if(w > 992 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });
});


$('.btn-search-toggle').click(function(e) {
    e.preventDefault();
    $('body').find('.search-modal').toggleClass('open');
});



$(".slider-large-item").fancybox({
    'padding'    : 0,
    fullScreen : true
});


$( ".form-input" ).focusin(function() {
    $(this).closest('.form-wrap').addClass('focus');
});

$( ".form-input").focusout(function() {

    var val =  $(this).val();
    console.log(val);

    if (!val) {
        $(this).closest('.form-wrap').removeClass('focus');
    }
});

// ----- Маска ----------
jQuery(function($){
    $("input[name='birthdate']").mask("99/99/9999");
});


$('.slider-source').slick({
    dots: true,
    infinite: true,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"><svg><use xlink:href="#prev"></use></svg></span>',
    nextArrow: '<span class="slide-nav next"><svg><use xlink:href="#next"></use></svg></span>',
    responsive: [
        {
            breakpoint: 992,
            settings: {
                arrows: false
            }
        }
    ]
});

$('.new-slide').slick({
    dots: false,
    infinite: false,
    arrows: false,
    slidesToShow: 4,
    slidesToScroll: 4,
    adaptiveHeight: false,
    prevArrow: '<span class="slide-nav prev"><svg><use xlink:href="#prev"></use></svg></span>',
    nextArrow: '<span class="slide-nav next"><svg><use xlink:href="#next"></use></svg></span>',
    responsive: [
        {
            breakpoint: 1350,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: true,
                arrows: true
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                dots: true,
                arrows: true
            }
        }
    ]
});


$('.product-slide').slick({
    dots: false,
    infinite: false,
    arrows: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"><svg><use xlink:href="#prev"></use></svg></span>',
    nextArrow: '<span class="slide-nav next"><svg><use xlink:href="#next"></use></svg></span>',
    responsive: [
        {
            breakpoint: 1350,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                dots: false,
                arrows: true
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                arrows: false
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});

$('.brand-view').click(function(e) {
    e.preventDefault();
    $(this).closest('.vendor-block').find('.vendors').toggleClass('open');
    $(this).toggleClass('open');
});

$('.look-like').click(function(e) {
    e.preventDefault();
    $(this).toggleClass('likeit');
});


$('select').selectric({
    disableOnMobile: false,
    responsive: true,
    maxHeight: 350
});


$("input[type='radio']").ionCheckRadio();
$("input[type='checkbox']").ionCheckRadio();



$('.product-info-title').click(function(e) {
    e.preventDefault();
    var box = $(this).closest('li');

    if (box.hasClass('open')) {

    }
    else {

        $(this).closest('ul').find('li').removeClass('open');
        $(this).closest('ul').find('.product-info-content ').slideUp('fast');

        box.find('.product-info-content ').slideDown('fast');
        box.addClass('open');
    }
});


$('input[name="view-option"]').change(function() {

    var inputVal = $('input[name="view-option"]:checked').val();

    console.log(inputVal);

    if (inputVal == 2) {
        $('body').find('.showcase').addClass('column');
    }
    else {
        $('body').find('.showcase').removeClass('column');
    }
});


/// Рекомендации

$('.rec-slider').slick({
    dots: false,
    infinite: false,
    arrows: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"><svg><use xlink:href="#prev"></use></svg></span>',
    nextArrow: '<span class="slide-nav next"><svg><use xlink:href="#next"></use></svg></span>',
    responsive: [
        {
            breakpoint: 1350,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }
    ]
});

$('.rec-slider-item a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.rec');

    box.find('.rec-tabs').removeClass('active');
    box.find(tab).addClass('active');

    box.find('.rec-slider-item a').removeClass('active');
    $(this).closest('a').addClass('active');
});

$('.tabs-nav li a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.tabs');

    console.log(tab);
    $(this).closest('.tabs-nav').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    box.find('.tabs-item').removeClass('active');
    box.find(tab).addClass('active');
});

$('.slider-large').slick({
    dots: false,
    infinite: false,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.slider-thumbs',
    prevArrow: '<span class="slide-nav prev"><svg><use xlink:href="#prev"></use></svg></span>',
    nextArrow: '<span class="slide-nav next"><svg><use xlink:href="#next"></use></svg></span>',
    responsive: [
        {
            breakpoint: 1350,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                arrows: true
            }
        }
    ]
});

$('.slider-thumbs').slick({
    dots: false,
    infinite: false,
    arrows: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    vertical: true,
    focusOnSelect: true,
    asNavFor: '.slider-large',
    prevArrow: '<span class="thumbs-nav prev"></span>',
    nextArrow: '<span class="thumbs-nav next"></span>'
});

// Фильтр


$('.filter-title, .filter-toggle').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('.filter-slide-toggle').toggleClass('open');
    $(this).closest('.filter-slide-toggle').find('.filter-body').slideToggle('fast');
});


$('.filter-name').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('li').toggleClass('open');
    $(this).closest('li').find('.filter-list').slideToggle('fast');
});


$(function() {
    var pull = $('.filter-group-toggle');
    var menu = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.toggleClass('open');
    });

    $(window).resize(function(){
        var w = $(window).width();
        if(w > 992 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });
});



jQuery(document).ready(function(){
    jQuery('.scrollbar-inner').scrollbar();
});


$(document).ready(function() {
    $('.minus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.plus').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });
});


$('.accordion-title').click(function(e) {
    e.preventDefault();
    var box = $(this).closest('li');

    if (box.hasClass('open')) {

    }
    else {

        $(this).closest('ul').find('li').removeClass('open');
        $(this).closest('ul').find('.accordion-content ').slideUp('fast');

        box.find('.accordion-content ').slideDown('fast');
        box.addClass('open');
    }
});