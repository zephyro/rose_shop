<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/topnav.inc.php') ?>
            <!-- -->

            <section class="account">
                <div class="container">

                    <h1>Личный кабинет</h1>

                    <div class="row">

                        <article class="account-content">

                           <div class="address-table">
                               <ul>
                                   <li>Адрес</li>
                                   <li>Телефон</li>
                                   <li></li>
                               </ul>
                               <ul>
                                   <li>Россия, г. Самара, ул. Карла Маркса 26 -  65</li>
                                   <li><span>+7 (989) 656-55-55</span></li>
                                   <li>
                                       <a href="#" class="btn-text">Изменить</a>
                                       <a href="#" class="btn-text">Удалить</a>
                                   </li>
                               </ul>
                               <ul>
                                   <li>Россия, г. Самара, ул. Карла Маркса 22</li>
                                   <li><span>+7 (989) 656-00-00</span></li>
                                   <li>
                                       <a href="#" class="btn-text">Изменить</a>
                                       <a href="#" class="btn-text">Удалить</a>
                                   </li>
                               </ul>
                           </div>
                            <a href="#" class="btn-text">Добавить адрес</a>

                        </article>

                        <aside class="account-sidebar">
                            <ul class="account-nav">
                                <li><a href="#"><span>Личные данные</span></a></li>
                                <li class="active"><a href="#"><span>Мои адреса</span></a></li>
                                <li><a href="#"><span>Мои покупки</span></a></li>
                                <li><a href="#"><span>Персональные настройки</span></a></li>
                                <li><a href="#"><span>Активация сертификата</span></a></li>
                                <li><a href="#"><span>Выйти</span></a></li>
                            </ul>
                        </aside>

                    </div>
                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>