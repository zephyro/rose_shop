<div class="toolbar">
    <div class="toolbar-value">279 товаров</div>
    <ul class="toolbar-option">
        <li>
            <div class="view-params">
                <label>
                    <input type="radio" name="view-params">
                    <span>на модели</span>
                </label>
            </div>
        </li>
        <li>
            <div class="view-params">
                <label>
                    <input type="radio" name="view-params" checked>
                    <span>вещь</span>
                </label>
            </div>
        </li>
        <li class="hidden-md hidden-lg">
            <div class="view-option option-col">
                <label>
                    <input type="radio" value="2" name="view-option">
                </label>
            </div>
        </li>
        <li class="hidden-md hidden-lg">
            <div class="view-option option-row">
                <label>
                    <input type="radio" value="1" name="view-option" checked>
                </label>
            </div>
        </li>
        <li class="hidden-xs hidden-sm">
            <select class="form-select">
                <option value="">Сортировать</option>
                <option value="">По цене</option>
                <option value="">По популярности</option>
                <option value="">По брендам</option>
            </select>
        </li>
    </ul>
</div>