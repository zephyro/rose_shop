<footer class="footer">
    <div class="footer-line"></div>
    <div class="container">
        <ul class="footer-row footer-top">
            <li class="hidden-xs hidden-sm">
                <a href="#" class="footer-logo">
                    <img src="img/header_logo.png" class="img-responsive" alt="">
                </a>
            </li>
            <li>
                <h4>Клиентам</h4>
                <ul class="footer-nav">
                    <li><a href="#">Контакты</a></li>
                    <li><a href="#">Доставка</a></li>
                    <li><a href="#">Оплата</a></li>
                    <li><a href="#">Обмен и возврат</a></li>
                    <li><a href="#">Консультация стилиста</a></li>
                    <li><a href="#">Самовывоз</a></li>
                </ul>
            </li>
            <li>
                <h4>О компании</h4>
                <ul class="footer-nav">
                    <li><a href="#">О нас</a></li>
                    <li><a href="#">Оферта</a></li>
                    <li><a href="#">Партнерам</a></li>
                </ul>
            </li>
            <li>
                <h4>Наши преимущества</h4>
                <ul class="footer-nav">
                    <li><a href="#">Мировые модные бренды онлайн</a></li>
                    <li><a href="#">Доставка по России и СНГ</a></li>
                    <li><a href="#">Бесплатный самовывоз</a></li>
                    <li><a href="#">Выбор удобного способа оплаты</a></li>
                    <li><a href="#">Консультации стилиста</a></li>
                    <li><a href="#">Гарантия качества</a></li>
                </ul>
            </li>
        </ul>

        <ul class="footer-row">
            <li class="hidden-xs hidden-sm">
                <div class="footer-title"></div>
                <p>© 2017 roselle - All rights reserved</p>
            </li>
            <li class="double-col">
                <div class="footer-title">Подпишитесь на нас</div>
                <ul class="footer-social">
                    <li><a href="#" class="social-vk"><span>vkontakte</span></a></li>
                    <li><a href="#" class="social-fb"><span>Facebook</span></a></li>
                    <li><a href="#" class="social-tl"><span>telegram</span></a></li>
                </ul>
            </li>
            <li>
                <div class="footer-title">Принимаем к оплате</div>

                <ul class="footer-payment">
                    <li>
                        <img src="svg/visa.svg" alt="" class="img-responsive">
                    </li>
                    <li>
                        <img src="svg/paypal.svg" alt="" class="img-responsive">
                    </li>
                    <li>
                        <img src="svg/mastercarg.svg" alt="" class="img-responsive">
                    </li>
                </ul>
            </li>
            <li class="hidden-md hidden-lg">
                <div class="footer-title"></div>
                <p>© 2017 roselle - All rights reserved</p>
            </li>
        </ul>

    </div>
</footer>

<!-- SVG -->

<div class="hide">
    <?php include('svg/heart.svg') ?>

    <?php include('svg/slide_nav_prev.svg') ?>

    <?php include('svg/slide_nav_next.svg') ?>
</div>
