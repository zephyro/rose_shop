<header class="header">
    <div class="container">
        <a class="header-logo" href="#">
            <img src="img/header_logo.png" class="img-responsive" alt="">
        </a>

        <ul class="header-left">
            <li class="hidden-md hidden-lg">
                <a class="btn-nav navbar-toggle" data-target=".topnav">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
            </li>
            <li>
                <a class="header-search btn-search-toggle" href="#"><span>поиск</span></a>
            </li>
        </ul>

        <ul class="header-right">
            <li class="hidden-xs hidden-sm">
                <a class="header-lk" href="#">
                    <?php include('svg/user.svg') ?>
                </a>
            </li>
            <li>
                <a class="header-like" href="#">
                    <span>10</span>
                    <?php include('svg/heart-checked.svg') ?>
                </a>
            </li>
            <li>
                <a class="header-basket basket-full" href="">
                    <?php include('svg/handbag.svg') ?>
                    <span>5</span>
                </a>
            </li>
        </ul>

    </div>
</header>

<div class="search-modal">
    <div class="container">
        <div class="search-title">Поиск</div>
        <div class="search-inner">
            <form class="form">
                <div class="search-form">
                    <input class="search-input" name="search" placeholder="Поиск по сайту">
                    <button class="btn-reset btn-search-toggle"></button>
                </div>
                <ul class="search-result">
                    <li><span>A.W.A.K.E.  (23)</span></li>
                    <li><span>ACNE STUDIO  (42)</span></li>
                    <li><span>ALESSANDRA RIC  (10)</span></li>
                    <li><span>CHARLOTTE OLYMPIA  (7)</span></li>
                    <li><span>CHRISTIAN LOUBOUTIN  (8)</span></li>
                    <li><span>ETRO  (12)</span></li>
                    <li><span>DELPOZO  (1)</span></li>
                    <li><span>ERDEM  (1)</span></li>
                    <li><span>GUCCI  (10)</span></li>
                    <li><span>HAIDER ACKERMANN  (0)</span></li>
                </ul>
            </form>
        </div>
    </div>
</div>