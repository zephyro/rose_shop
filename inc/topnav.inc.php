<nav class="topnav">
    <div class="container">
        <span class="btn-nav-close navbar-toggle" data-target=".topnav">
            <?php include('svg/close.svg') ?>
        </span>

        <ul class="nav-primary">
            <li class="dropdown">
                <a href="#"><span>Новинки</span></a>
                <div class="nav-second">
                    <div class="nav-second-title">Новинки</div>
                    <ul>
                        <li><a href="#">Верхняя одежда</a></li>
                        <li><a href="#">Джинсы</a></li>
                        <li><a href="#">Жакеты / Жилеты</a></li>
                        <li><a href="#">Комбинезоны</a></li>
                        <li><a href="#">Комплекты</a></li>
                        <li><a href="#">Платья</a></li>
                    </ul>
                </div>
            </li>
            <li class="dropdown">
                <a href="#"><span>Бренды</span></a>
                <div class="nav-second">

                    <div class="nav-second-column hidden-xs hidden-sm">
                        <div class="nav-second-title">Новинки</div>
                        <ul>
                            <li><a href="#">ОДЕЖДА</a></li>
                            <li><a href="#">ОБУВЬ</a></li>
                            <li><a href="#">СУМКИ</a></li>
                            <li><a href="#">АКСЕССУАРЫ</a></li>
                            <li><a href="#">НИЖНЕЕ БЕЛЬЕ</a></li>
                            <li><a href="#">КРАСОТА</a></li>
                            <li><a href="#">ПОДАРКИ</a></li>
                            <li><a href="#">ВИНТАЖ</a></li>
                        </ul>
                    </div>

                    <div class="nav-second-column nav-second-inline">
                        <div class="nav-second-title">Бренды</div>
                        <ul>
                            <li><a href="#">3.1 PHILLIP LIM</a></li>
                            <li><a href="#">A.W.A.K.E.</a></li>
                            <li><a href="#">ACNE STUDIOS</a></li>
                            <li><a href="#">ALESSANDRA RICH</a></li>
                            <li><a href="#">CHARLOTTE OLYMPIA</a></li>
                            <li><a href="#">CHRISTIAN LOUBOUTIN</a></li>
                            <li><a href="#">ETRO</a></li>
                            <li><a href="#">DELPOZO</a></li>
                            <li><a href="#">ERDEM</a></li>
                            <li><a href="#">GUCCI</a></li>
                            <li><a href="#">HAIDER ACKERMANN</a></li>
                            <li><a href="#">ISABEL MARANT</a></li>
                            <li><a href="#">ISABEL MARANT ETOILE</a></li>
                            <li><a href="#">MARC JACOBS</a></li>
                            <li><a href="#">NATASHA ZINKO</a></li>
                            <li><a href="#">NINA RICCI</a></li>
                            <li><a href="#">NO.21</a></li>
                            <li><a href="#">PROENZA SCHOULER</a></li>
                            <li><a href="#">ROCHAS</a></li>
                            <li><a href="#">SIES MARJAN</a></li>
                            <li><a href="#">SELF-PORTRAIT</a></li>
                            <li><a href="#">VALENTINO</a></li>
                        </ul>
                    </div>

                    <div class="single-brand">
                        <a href="#" class="single-brand-image">
                            <img src="img/brand.jpg" class="img-responsive" alt="">
                        </a>

                        <div class="single-brand-name">DELPOZO</div>
                        <a href="#" class="go-brand">Перейти к бренду</a>
                    </div>

                </div>
            </li>
            <li><a href="#"><span>Обувь</span></a></li>
            <li><a href="#"><span>Женское</span></a></li>
            <li><a href="#"><span>Сумки</span></a></li>
            <li><a href="#"><span>Аксессуары</span></a></li>
            <li><a href="#"><span>Мужское</span></a></li>
            <li><a href="#"><span>Детское</span></a></li>
            <li><a href="#"><span>Подарочные карты</span></a></li>
        </ul>
    </div>
</nav>