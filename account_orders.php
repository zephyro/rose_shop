<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/topnav.inc.php') ?>
            <!-- -->

            <section class="account">
                <div class="container">

                    <h1>Личный кабинет</h1>

                    <div class="row">

                        <article class="account-content">

                            <h4>Последние заказы</h4>

                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tr>
                                        <th>№ заказа</th>
                                        <th>Дата заказа</th>
                                        <th>Статус</th>
                                        <th>Дата отправки</th>
                                        <th>Детали</th>
                                    </tr>
                                    <tr>
                                        <td>25673652</td>
                                        <td>11 / 06 / 2017</td>
                                        <td>Обработка заказа</td>
                                        <td></td>
                                        <td><a href="#">Посмотреть заказ</a></td>
                                    </tr>
                                    <tr>
                                        <td>25673652</td>
                                        <td>11 / 06 / 2017</td>
                                        <td>Обработка заказа</td>
                                        <td></td>
                                        <td><a href="#">Посмотреть заказ</a></td>
                                    </tr>
                                    <tr>
                                        <td>25673652</td>
                                        <td>11 / 06 / 2017</td>
                                        <td><a href="#">Отследить этот заказ</a></td>
                                        <td>11 / 06 / 2017</td>
                                        <td><a href="#">Посмотреть заказ</a></td>
                                    </tr>
                                    <tr>
                                        <td>25673652</td>
                                        <td>11 / 06 / 2017</td>
                                        <td>Обработка заказа</td>
                                        <td>11 / 06 / 2017</td>
                                        <td><a href="#">Посмотреть заказ</a></td>
                                    </tr>
                                </table>
                            </div>

                        </article>

                        <aside class="account-sidebar">
                            <ul class="account-nav">
                                <li><a href="#"><span>Личные данные</span></a></li>
                                <li class="active"><a href="#"><span>Мои адреса</span></a></li>
                                <li><a href="#"><span>Мои покупки</span></a></li>
                                <li><a href="#"><span>Персональные настройки</span></a></li>
                                <li><a href="#"><span>Активация сертификата</span></a></li>
                                <li><a href="#"><span>Выйти</span></a></li>
                            </ul>
                        </aside>

                    </div>
                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>