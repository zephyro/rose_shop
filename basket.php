<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/topnav.inc.php') ?>
            <!-- -->


            <section class="main">
                <div class="container">

                    <ul class="switch-nav">
                       <li class="active"><a href="#"><span>Корзина</span></a></li>
                       <li><a href="#"><span>Избранное</span></a></li>
                    </ul>

                    <div class="mobile-button">
                        <button class="btn btn-black">оформить заказ</button>
                    </div>

                    <div class="row">
                        <article class="content-basket">

                            <ul class="basket-row">

                                <li class="basket-row-heading">
                                    <div class="basket">
                                        <div class="basket-image">
                                            <span class="basket-heading-text">Товар</span>
                                        </div>
                                        <ul class="basket-content">
                                            <li class="basket-name"><li>
                                            <li class="basket-size"><span class="basket-heading-text">Размер</span></li>
                                            <li class="basket-color"><span class="basket-heading-text">Цвет</span></li>
                                            <li class="basket-number"><span class="basket-heading-text">Количество</span></li>
                                            <li class="basket-price"><span class="basket-heading-text">Цена</span></li>
                                        </ul>
                                    </div>
                                </li>

                                <li>
                                    <span class="basket-item-close"><?php include('svg/cansel.svg') ?></span>
                                    <div class="basket">
                                        <div class="basket-image">
                                            <a href="#">
                                                <img src="img/new/pr_04.jpg" class="img-responsive" alt="">
                                            </a>
                                        </div>
                                        <ul class="basket-content">
                                            <li class="basket-name">
                                                <div class="basket-brand">valentino</div>
                                                <div class="basket-product">ПЛАТЬЕ ИЗ ГИПЮРА И ПАРЧИ</div>
                                                <div class="basket-like">
                                                    <?php include('inc/heart.inc.php') ?>
                                                    <span class="basket-like-text">Добавить в избранное</span>
                                                </div>
                                            </li>
                                            <li class="basket-size">
                                                <span class="basket-row-title">Размер</span>
                                                <span class="basket-text">L|48</span>
                                            </li>
                                            <li class="basket-color">
                                                <span class="basket-row-title">Цвет</span>
                                                <span class="basket-text">черный</span>
                                            </li>
                                            <li class="basket-number">
                                                <span class="basket-row-title">Количество</span>
                                                <div class="input-number">
                                                    <span class="plus"></span>
                                                    <input type="text" value="1" class="value">
                                                    <span class="minus"></span>
                                                </div>
                                            </li>
                                            <li class="basket-price">
                                                <span class="basket-price-value">29 100 Р</span>
                                                <div class="basket-price-text">-1620 Р скидка при оплате онлайн</div>
                                            </li>
                                        </ul>

                                    </div>
                                </li>

                                <li>
                                    <span class="basket-item-close"><?php include('svg/cansel.svg') ?></span>
                                    <div class="basket">
                                        <div class="basket-image">
                                            <a href="#">
                                                <img src="img/new/pr_04.jpg" class="img-responsive" alt="">
                                            </a>
                                        </div>
                                        <ul class="basket-content">
                                            <li class="basket-name">
                                                <div class="basket-brand">valentino</div>
                                                <div class="basket-product">ПЛАТЬЕ ИЗ ГИПЮРА И ПАРЧИ</div>
                                                <div class="basket-like">
                                                    <?php include('inc/heart.inc.php') ?>
                                                    <span class="basket-like-text">Добавить в избранное</span>
                                                </div>
                                            </li>
                                            <li class="basket-size">
                                                <span class="basket-row-title">Размер</span>
                                                <span class="basket-text">L|48</span>
                                            </li>
                                            <li class="basket-color">
                                                <span class="basket-row-title">Цвет</span>
                                                <span class="basket-text">черный</span>
                                            </li>
                                            <li class="basket-number">
                                                <span class="basket-row-title">Количество</span>
                                                <div class="input-number">
                                                    <span class="plus"></span>
                                                    <input type="text" value="1" class="value">
                                                    <span class="minus"></span>
                                                </div>
                                            </li>
                                            <li class="basket-price">
                                                <span class="basket-price-value">29 100 Р</span>
                                                <div class="basket-price-text">-1620 Р скидка при оплате онлайн</div>
                                            </li>
                                        </ul>

                                    </div>
                                </li>

                                <li>
                                    <span class="basket-item-close"><?php include('svg/cansel.svg') ?></span>
                                    <div class="basket">
                                        <div class="basket-image">
                                            <a href="#">
                                                <img src="img/new/pr_04.jpg" class="img-responsive" alt="">
                                            </a>
                                        </div>
                                        <ul class="basket-content">
                                            <li class="basket-name">
                                                <div class="basket-brand">valentino</div>
                                                <div class="basket-product">ПЛАТЬЕ ИЗ ГИПЮРА И ПАРЧИ</div>
                                                <div class="basket-like">
                                                    <?php include('inc/heart.inc.php') ?>
                                                    <span class="basket-like-text">Добавить в избранное</span>
                                                </div>
                                            </li>
                                            <li class="basket-size">
                                                <span class="basket-row-title">Размер</span>
                                                <span class="basket-text">L|48</span>
                                            </li>
                                            <li class="basket-color">
                                                <span class="basket-row-title">Цвет</span>
                                                <span class="basket-text">черный</span>
                                            </li>
                                            <li class="basket-number">
                                                <span class="basket-row-title">Количество</span>
                                                <div class="input-number">
                                                    <span class="plus"></span>
                                                    <input type="text" value="1" class="value">
                                                    <span class="minus"></span>
                                                </div>
                                            </li>
                                            <li class="basket-price">
                                                <span class="basket-price-value">29 100 Р</span>
                                                <div class="basket-price-text">-1620 Р скидка при оплате онлайн</div>
                                            </li>
                                        </ul>

                                    </div>
                                </li>

                            </ul>

                            <div class="product-list hidden-xs hidden-sm">
                                <h2>Вы недавно смотрели</h2>

                                <div class="product-list-row">

                                    <div class="product-list-item">
                                        <div class="showcase-item">
                                            <div class="showcase-image">
                                                <a href="#">
                                                    <img src="img/look/look_04.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="showcase-text">
                                                <a href="#">
                                                    <div class="vendor">valentino</div>
                                                    <div class="price">€ 3 120,00</div>
                                                    <?php include('inc/heart.inc.php') ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="product-list-item">
                                        <div class="showcase-item">
                                            <div class="showcase-image">
                                                <a href="#">
                                                    <img src="img/look/look_02.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="showcase-text">
                                                <a href="#">
                                                    <div class="vendor">valentino</div>
                                                    <div class="price">€ 3 120,00</div>
                                                    <?php include('inc/heart.inc.php') ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="product-list-item">
                                        <div class="showcase-item">
                                            <div class="showcase-image">
                                                <a href="#">
                                                    <img src="img/look/look_03.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="showcase-text">
                                                <a href="#">
                                                    <div class="vendor">valentino</div>
                                                    <div class="price">€ 3 120,00</div>
                                                    <?php include('inc/heart.inc.php') ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="product-list-item">
                                        <div class="showcase-item">
                                            <div class="showcase-image">
                                                <a href="#">
                                                    <img src="img/look/look_03.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="showcase-text">
                                                <a href="#">
                                                    <div class="vendor">valentino</div>
                                                    <div class="price">€ 3 120,00</div>
                                                    <?php include('inc/heart.inc.php') ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </article>

                        <aside class="aside">

                            <div class="side-action">
                                <a href="#" class="action-print">
                                    <i>
                                        <?php include('svg/print.svg') ?>
                                    </i>
                                    <span>Распечатать</span>
                                </a>
                                <a href="#" class="action-send">
                                    <i>
                                        <?php include('svg/send.svg') ?>
                                    </i>
                                    <span>Отправить</span>
                                </a>
                            </div>

                            <div class="side-box gray-box">
                                <form class="form">
                                    <div class="form-group">
                                        <input type="text" class="form-line" placeholder="> Промо-код на скидку">
                                    </div>
                                    <div class="box-summary">
                                        <span class="box-summary-legend">Сумма:</span>
                                        <span class="box-summary-value"><strong>84 200</strong> р</span>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-black">ОФОРМИТЬ ЗАКАЗ</button>
                                    </div>
                                    <div class="info-text">Вы можете оформить заказ только на товары, которые есть в наличии, для остальных воспользуйтесь функцией «Отложить в избранное»</div>
                                </form>
                            </div>

                            <div class="side-box gray-box">
                                <div class="side-title">Дополнительная информация</div>

                                <ul class="accordion">
                                    <li class="open">
                                        <div class="accordion-title">
                                            <span>Служба поддержки</span>
                                            <i></i>
                                        </div>
                                        <div class="accordion-content">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique enim vel nunc tristique accumsan. Etiam et nibh ultrices, imperdiet mauris et, congue nisl
                                        </div>
                                    </li>
                                    <li>
                                        <div class="accordion-title">
                                            <span>Доступные способы оплаты</span>
                                            <i></i>
                                        </div>
                                        <div class="accordion-content">
                                            <h4>Оплата онлайн:</h4>
                                            <ul class="payment">
                                                <li class="p-visa">
                                                    <img src="svg/visa.svg" alt="" class="img-responsive">
                                                </li>
                                                <li class="p-paypal">
                                                    <img src="svg/paypal.svg" alt="" class="img-responsive">
                                                </li>
                                                <li class="p-ms">
                                                    <img src="svg/mastercarg.svg" alt="" class="img-responsive">
                                                </li>
                                                <li class="p-ipay">
                                                    <img src="svg/ipay.svg" alt="" class="img-responsive">
                                                </li>
                                            </ul>
                                            <h4>Оплата при получении:</h4>
                                            <p>Вы можете оплатить товар наличными или картой курьеру при получении</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="accordion-title">
                                            <span>Варианты доставки</span>
                                            <i></i>
                                        </div>
                                        <div class="accordion-content">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique enim vel nunc tristique accumsan. Etiam et nibh ultrices, imperdiet mauris et, congue nisl
                                        </div>
                                    </li>

                                </ul>

                            </div>

                        </aside>
                    </div>

                </div>

            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>