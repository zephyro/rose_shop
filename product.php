<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/topnav.inc.php') ?>
            <!-- -->


            <section class="main">
                <div class="container">

                    <div class="product">

                        <div class="product-gallery">
                            
                            <div class="product-images">

                                <div class="product-images-thumbs">
                                    <div class="slider-thumbs">

                                        <div class="slider-thumbs-item">
                                            <div class="thumbs-inner">
                                                <img src="img/product.jpg" class="img-responsive" alt="">
                                            </div>
                                        </div>

                                        <div class="slider-thumbs-item">
                                            <div class="thumbs-inner">
                                                <img src="img/product.jpg" class="img-responsive" alt="">
                                            </div>
                                        </div>

                                        <div class="slider-thumbs-item">
                                            <div class="thumbs-inner">
                                                <img src="img/product.jpg" class="img-responsive" alt="">
                                            </div>
                                        </div>

                                        <div class="slider-thumbs-item">
                                            <div class="thumbs-inner">
                                                <div class="product-video">
                                                    <img src="img/product.jpg" class="img-responsive" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-images-large">

                                    <div class="slider-large">

                                        <a  class="slider-large-item" href="img/product.jpg">
                                            <img src="img/product.jpg" class="img-responsive" alt="">
                                        </a>

                                        <a class="slider-large-item" href="img/product.jpg">
                                            <img src="img/product.jpg" class="img-responsive" alt="">
                                        </a>

                                        <a  class="slider-large-item" href="img/product.jpg">
                                            <img src="img/product.jpg" class="img-responsive" alt="">
                                        </a>

                                        <a  data-fancybox href="https://www.youtube.com/embed/Q8xDjZEi04o&autoplay=1&rel=0&showinfo=0">
                                            <div class="product-video">
                                                <img src="img/product.jpg" class="img-responsive" alt="">
                                            </div>
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="clearfix">
                                <ul class="product-sm-action hidden-md hidden-lg no-video">
                                    <li>
                                        <a href="#" class="btn-product-share">
                                            <?php include('svg/share.svg') ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-fancybox href="https://www.youtube.com/embed/Q8xDjZEi04o&autoplay=1&rel=0&showinfo=0"  class="btn-play"><span>видео</span></a>
                                    </li>
                                    <li>
                                        <?php include('inc/heart.inc.php') ?>
                                    </li>
                                </ul>

                                <ul class="product-share hidden-xs hidden-sm">
                                    <li>Поделиться</li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-vk"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-telegram"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="product-content">

                            <ul class="breadcrumbs hidden-xs hidden-sm">
                                <li><a href="#">женщинам</a></li>
                                <li><a href="#">Трикотаж</a></li>
                                <li><span>valentino</span></li>
                            </ul>

                            <div class="product-brand">valentino</div>

                            <div class="product-name">ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ С ПРИНТОМ</div>

                            <div class="product-price">€ 3 120,00</div>

                            <div class="product-color hidden-xs hidden-sm">
                                <span>Другие цвета:</span>
                                <ul>
                                    <li>
                                        <label class="icr-label">
                                            <span class="icr-item type_radio"></span>
                                            <span class="icr-hidden"><input class="icr-input" type="radio" name="job" value="0" checked /></span>
                                            <span class="icr-text">
                                                <img src="img/pattern_brown.png" class="img-responsive">
                                            </span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="icr-label">
                                            <span class="icr-item type_radio"></span>
                                            <span class="icr-hidden"><input class="icr-input" type="radio" name="job" value="0" /></span>
                                            <span class="icr-text">
                                                <img src="img/pattern_white.png" class="img-responsive">
                                            </span>
                                        </label>

                                    </li>
                                </ul>
                            </div>

                            <div class="product-size">
                                <ul class="product-row">
                                    <li>
                                        <select class="form-select">
                                            <option>Выберете размер</option>
                                            <option value="">XS</option>
                                            <option value="">SM</option>
                                            <option value="">M</option>
                                            <option value="">L</option>
                                            <option value="">XL</option>
                                        </select>
                                    </li>
                                    <li></li>
                                </ul>
                            </div>

                            <div class="product-question ">
                                <span>Не нашли свой размер?</span> <a href="#">Уведомить о поступлении</a> <span class="hidden-md hidden-lg">Размерность соответствует стандарту</span>
                            </div>

                            <div class="product-buy">
                                <ul class="product-row">
                                    <li>
                                        <a href="#" class="btn btn-black">добавить в корзину</a>
                                    </li>
                                    <li>
                                        <a href="#" class="btn">купить весь look</a>
                                    </li>
                                    <li class="hidden-xs hidden-sm">
                                        <?php include('inc/heart.inc.php') ?>
                                    </li>
                                </ul>
                            </div>

                            <ul class="product-info">
                                <li class="open">
                                    <div class="product-info-title">
                                        <span>Комментарий стилиста</span>
                                        <i></i>
                                    </div>
                                    <div class="product-info-content">
                                        Платье из хлопковой рогожки с принтом "Кубинский цветок".<br/>
                                        Сзади: потайная застёжка-молния и крючок.<br/>
                                        Юбка в складку с контрастной изнанкой.<br/>
                                        Длина: 123 см.<br/>
                                        Лиф на подкладке из хлопкового муслина (100% хлопок).<br/>
                                        Хлопковая рогожка (80% хлопок, 20% лён).<br/>
                                        Классический силуэт.<br/>
                                        Модель на фотографии: рост 176 см, изделие 40-го размера.<br/>
                                        Производство: Италия.
                                    </div>
                                </li>
                                <li>
                                    <div class="product-info-title">
                                        <span>Детали</span>
                                        <i></i>
                                    </div>
                                    <div class="product-info-content">
                                        Платье из хлопковой рогожки с принтом "Кубинский цветок".<br/>
                                        Сзади: потайная застёжка-молния и крючок.<br/>
                                        Юбка в складку с контрастной изнанкой.<br/>
                                        Длина: 123 см.<br/>
                                        Лиф на подкладке из хлопкового муслина (100% хлопок).<br/>
                                        Хлопковая рогожка (80% хлопок, 20% лён).<br/>
                                        Классический силуэт.<br/>
                                        Модель на фотографии: рост 176 см, изделие 40-го размера.<br/>
                                        Производство: Италия.<br/>
                                    </div>
                                </li>
                                <li>
                                    <div class="product-info-title">
                                        <span>Размер и крой</span>
                                        <i></i>
                                    </div>
                                    <div class="product-info-content">
                                        Платье из хлопковой рогожки с принтом "Кубинский цветок".<br/>
                                        Сзади: потайная застёжка-молния и крючок.<br/>
                                        Юбка в складку с контрастной изнанкой.<br/>
                                        Длина: 123 см.<br/>
                                        Лиф на подкладке из хлопкового муслина (100% хлопок).<br/>
                                        Хлопковая рогожка (80% хлопок, 20% лён).<br/>
                                        Классический силуэт.<br/>
                                        Модель на фотографии: рост 176 см, изделие 40-го размера.<br/>
                                        Производство: Италия.
                                    </div>
                                </li>
                                <li>
                                    <div class="product-info-title">
                                        <span>Доставка и оплата</span>
                                        <i></i>
                                    </div>
                                    <div class="product-info-content">
                                        Платье из хлопковой рогожки с принтом "Кубинский цветок".<br/>
                                        Сзади: потайная застёжка-молния и крючок.<br/>
                                        Юбка в складку с контрастной изнанкой.<br/>
                                        Длина: 123 см.<br/>
                                        Лиф на подкладке из хлопкового муслина (100% хлопок).<br/>
                                        Хлопковая рогожка (80% хлопок, 20% лён).<br/>
                                        Классический силуэт.<br/>
                                        Модель на фотографии: рост 176 см, изделие 40-го размера.<br/>
                                        Производство: Италия.
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>

                    <div class="rec">

                        <h3>
                            <span class="hidden-xs hidden-sm">Стилист Roselle рекомендует сочетать платье Valentino с</span>
                            <span class="hidden-md hidden-lg">Создать образ</span>
                        </h3>

                        <div class="rec-slider">
                            <div class="rec-slider-item">
                                <a href="#" data-target=".rec01" class="active">
                                    <img src="img/obraz_01.jpg" class="img-responsive">
                                </a>
                            </div>
                            <div class="rec-slider-item">
                                <a href="#" data-target=".rec02">
                                    <img src="img/obraz_02.jpg" class="img-responsive">
                                </a>
                            </div>
                            <div class="rec-slider-item">
                                <a href="#" data-target=".rec03">
                                    <img src="img/obraz_01.jpg" class="img-responsive">
                                </a>
                            </div>
                            <div class="rec-slider-item">
                                <a href="#" data-target=".rec04">
                                    <img src="img/obraz_02.jpg" class="img-responsive">
                                </a>
                            </div>
                            <div class="rec-slider-item">
                                <a href="#" data-target=".rec05">
                                    <img src="img/obraz_01.jpg" class="img-responsive">
                                </a>
                            </div>
                            <div class="rec-slider-item">
                                <a href="#" data-target=".rec06">
                                    <img src="img/obraz_02.jpg" class="img-responsive">
                                </a>
                            </div>
                        </div>


                        <div class="rec-tabs rec01 active">

                            <div class="rec-look">

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_03.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_04.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_03.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="rec-tabs rec02">

                            <div class="rec-look">

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_03.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_04.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="rec-tabs rec03">

                            <div class="rec-look">

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_03.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_04.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="rec-tabs rec04">

                            <div class="rec-look">

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_03.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_04.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="rec-tabs rec05">

                            <div class="rec-look">

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_03.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_04.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="rec-tabs rec06">

                            <div class="rec-look">

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_04.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_03.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="rec-look-item">

                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>


                    </div>


                    <div class="browsed hidden-xs hidden-sm">

                        <div class="tabs">

                            <ul class="browsed-nav tabs-nav">
                                <li class="active"><a href="#" data-target=".tab1">Новинки</a></li>
                                <li><a href="#" data-target=".tab2">Вы недавно смотрели</a></li>
                                <li><a href="#" data-target=".tab3">Вам может понравиться</a></li>
                            </ul>

                            <div class="tabs-item tab1 active">
                                <div class="browsed-row">
                                    <div class="browsed-item">
                                        <div class="showcase-item">
                                            <div class="showcase-image">
                                                <a href="#">
                                                    <img src="img/new/pr_01.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="showcase-text">
                                                <a href="#">
                                                    <div class="vendor">valentino</div>
                                                    <h4>ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ</h4>
                                                    <div class="price">€ 3 120,00</div>
                                                </a>
                                            </div>
                                            <?php include('inc/heart.inc.php') ?>
                                        </div>
                                    </div>
                                    <div class="browsed-item">
                                        <div class="showcase-item">
                                            <div class="showcase-image">
                                                <a href="#">
                                                    <img src="img/new/pr_02.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="showcase-text">
                                                <a href="#">
                                                    <div class="vendor">valentino</div>
                                                    <h4>ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ</h4>
                                                    <div class="price">€ 3 120,00</div>
                                                </a>
                                            </div>
                                            <?php include('inc/heart.inc.php') ?>
                                        </div>
                                    </div>
                                    <div class="browsed-item">
                                        <div class="showcase-item">
                                            <div class="showcase-image">
                                                <a href="#">
                                                    <img src="img/new/pr_03.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="showcase-text">
                                                <a href="#">
                                                    <div class="vendor">valentino</div>
                                                    <h4>ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ</h4>
                                                    <div class="price">€ 3 120,00</div>
                                                </a>
                                            </div>
                                            <?php include('inc/heart.inc.php') ?>
                                        </div>
                                    </div>
                                    <div class="browsed-item">
                                        <div class="showcase-item">
                                            <div class="showcase-image">
                                                <a href="#">
                                                    <img src="img/new/pr_04.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="showcase-text">
                                                <a href="#">
                                                    <div class="vendor">valentino</div>
                                                    <h4>ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ</h4>
                                                    <div class="price">€ 3 120,00</div>
                                                </a>
                                            </div>
                                            <?php include('inc/heart.inc.php') ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tabs-item tab2">
                                <div class="browsed-row">
                                    <div class="browsed-item">
                                        <div class="showcase-item">
                                            <div class="showcase-image">
                                                <a href="#">
                                                    <img src="img/new/pr_03.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="showcase-text">
                                                <a href="#">
                                                    <div class="vendor">valentino</div>
                                                    <h4>ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ</h4>
                                                    <div class="price">€ 3 120,00</div>
                                                </a>
                                            </div>
                                            <?php include('inc/heart.inc.php') ?>
                                        </div>
                                    </div>
                                    <div class="browsed-item">
                                        <div class="showcase-item">
                                            <div class="showcase-image">
                                                <a href="#">
                                                    <img src="img/new/pr_04.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="showcase-text">
                                                <a href="#">
                                                    <div class="vendor">valentino</div>
                                                    <h4>ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ</h4>
                                                    <div class="price">€ 3 120,00</div>
                                                </a>
                                            </div>
                                            <?php include('inc/heart.inc.php') ?>
                                        </div>
                                    </div>
                                    <div class="browsed-item">
                                        <div class="showcase-item">
                                            <div class="showcase-image">
                                                <a href="#">
                                                    <img src="img/new/pr_01.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="showcase-text">
                                                <a href="#">
                                                    <div class="vendor">valentino</div>
                                                    <h4>ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ</h4>
                                                    <div class="price">€ 3 120,00</div>
                                                </a>
                                            </div>
                                            <?php include('inc/heart.inc.php') ?>
                                        </div>
                                    </div>
                                    <div class="browsed-item">
                                        <div class="showcase-item">
                                            <div class="showcase-image">
                                                <a href="#">
                                                    <img src="img/new/pr_02.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="showcase-text">
                                                <a href="#">
                                                    <div class="vendor">valentino</div>
                                                    <h4>ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ</h4>
                                                    <div class="price">€ 3 120,00</div>
                                                </a>
                                            </div>
                                            <?php include('inc/heart.inc.php') ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tabs-item tab3">
                                <div class="browsed-row">
                                    <div class="browsed-item">
                                        <div class="showcase-item">
                                            <div class="showcase-image">
                                                <a href="#">
                                                    <img src="img/new/pr_01.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="showcase-text">
                                                <a href="#">
                                                    <div class="vendor">valentino</div>
                                                    <h4>ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ</h4>
                                                    <div class="price">€ 3 120,00</div>
                                                </a>
                                            </div>
                                            <?php include('inc/heart.inc.php') ?>
                                        </div>
                                    </div>
                                    <div class="browsed-item">
                                        <div class="showcase-item">
                                            <div class="showcase-image">
                                                <a href="#">
                                                    <img src="img/new/pr_02.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="showcase-text">
                                                <a href="#">
                                                    <div class="vendor">valentino</div>
                                                    <h4>ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ</h4>
                                                    <div class="price">€ 3 120,00</div>
                                                </a>
                                            </div>
                                            <?php include('inc/heart.inc.php') ?>
                                        </div>
                                    </div>
                                    <div class="browsed-item">
                                        <div class="showcase-item">
                                            <div class="showcase-image">
                                                <a href="#">
                                                    <img src="img/new/pr_03.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="showcase-text">
                                                <a href="#">
                                                    <div class="vendor">valentino</div>
                                                    <h4>ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ</h4>
                                                    <div class="price">€ 3 120,00</div>
                                                </a>
                                            </div>
                                            <?php include('inc/heart.inc.php') ?>
                                        </div>
                                    </div>
                                    <div class="browsed-item">
                                        <div class="showcase-item">
                                            <div class="showcase-image">
                                                <a href="#">
                                                    <img src="img/new/pr_04.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="showcase-text">
                                                <a href="#">
                                                    <div class="vendor">valentino</div>
                                                    <h4>ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ</h4>
                                                    <div class="price">€ 3 120,00</div>
                                                </a>
                                            </div>
                                            <?php include('inc/heart.inc.php') ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->
        </div>

        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>
