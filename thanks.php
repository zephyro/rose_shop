<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <header class="header-empty">
                <div class="container">
                    <a class="header-logo" href="#">
                        <img src="img/header_logo.png" class="img-responsive" alt="">
                    </a>
                </div>
            </header>
            <!-- -->

            <section class="main">
                <div class="container">
                    <div class="thanks-content">
                        <h2>Спасибо за ваш заказ!</h2>
                        <p>Наш менеджер свяжется с вами в ближайшее время</p>
                    </div>

                    <div class="social">
                        <p>Актуальные новости в наших<br/>социальных сетях</p>
                        <ul class="social-list">
                            <li><a href="#" class="social-ins"></a></li>
                            <li><a href="#" class="social-vk"></a></li>
                            <li><a href="#" class="social-fb"></a></li>
                        </ul>
                    </div>


                </div>
            </section>

        </div>

        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>