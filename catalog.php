<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/topnav.inc.php') ?>
            <!-- -->


            <section class="main main-cat">
                <div class="container">
                    <ul class="mobile-nav">
                        <li>
                            <a href="#" class="btn-border filter-group-toggle" data-target=".filter-group">фильтровать</a>
                        </li>
                        <li>
                            <select class="form-select">
                                <option value="">Сортировать</option>
                                <option value="">По цене</option>
                                <option value="">По популярности</option>
                                <option value="">По брендам</option>
                            </select>
                        </li>
                    </ul>


                    <div class="row">

                        <!-- Filter -->
                        <div class="sidebar">

                            <?php include('inc/filter.inc.php') ?>

                        </div>
                        <!-- -->

                        <!-- Content -->
                        <div class="content">

                            <ul class="breadcrumbs hidden-xs hidden-sm"> 
                                <li><a href="#">женщинам</a></li>
                                <li><a href="#">Трикотаж</a></li>
                                <li><span>valentino</span></li>
                            </ul>

                            <h1 class="text-uppercase mb0">ВЕРХНЯЯ ОДЕЖДА</h1>

                            <!-- Catalog -->
                            <?php include('inc/toolbar.inc.php') ?>
                            <!-- -->

                            <!-- Catalog -->
                            <?php include('inc/catalog.inc.php') ?>
                            <!-- -->

                            <!-- Pagination -->
                            <?php include('inc/pagination.inc.php') ?>
                            <!-- -->


                        </div>
                        <!-- -->
                    </div>

                    <div class="browsed">
                        <h2>Вы недавно смотрели</h2>
                        <div class="new-slide">

                            <div class="product-slide-item">

                                <div class="showcase-item">
                                    <div class="showcase-image">
                                        <a href="#">
                                            <img src="img/new/pr_01.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="showcase-text">
                                        <a href="#">
                                            <div class="vendor">valentino</div>
                                            <h4>ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ</h4>
                                            <div class="price">€ 3 120,00</div>
                                        </a>
                                    </div>
                                    <?php include('inc/heart.inc.php') ?>
                                </div>

                            </div>

                            <div class="product-slide-item">

                                <div class="showcase-item">
                                    <div class="showcase-image">
                                        <a href="#">
                                            <img src="img/new/pr_02.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="showcase-text">
                                        <a href="#">
                                            <div class="vendor">valentino</div>
                                            <h4>ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ</h4>
                                            <div class="price">€ 3 120,00</div>
                                        </a>
                                    </div>
                                    <?php include('inc/heart.inc.php') ?>
                                </div>

                            </div>

                            <div class="product-slide-item">

                                <div class="showcase-item">
                                    <div class="showcase-image">
                                        <a href="#">
                                            <img src="img/new/pr_03.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="showcase-text">
                                        <a href="#">
                                            <div class="vendor">valentino</div>
                                            <h4>ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ</h4>
                                            <div class="price">€ 3 120,00</div>
                                        </a>
                                    </div>
                                    <?php include('inc/heart.inc.php') ?>
                                </div>

                            </div>

                            <div class="product-slide-item">

                                <div class="showcase-item">
                                    <div class="showcase-image">
                                        <a href="#">
                                            <img src="img/new/pr_04.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="showcase-text">
                                        <a href="#">
                                            <div class="vendor">valentino</div>
                                            <h4>ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ</h4>
                                            <div class="price">€ 3 120,00</div>
                                        </a>
                                    </div>
                                    <?php include('inc/heart.inc.php') ?>
                                </div>

                            </div>

                            <div class="product-slide-item">

                                <div class="showcase-item">
                                    <div class="showcase-image">
                                        <a href="#">
                                            <img src="img/new/pr_03.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="showcase-text">
                                        <a href="#">
                                            <div class="vendor">valentino</div>
                                            <h4>ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ</h4>
                                            <div class="price">€ 3 120,00</div>
                                        </a>
                                    </div>
                                    <?php include('inc/heart.inc.php') ?>
                                </div>

                            </div>

                            <div class="product-slide-item">

                                <div class="showcase-item">
                                    <div class="showcase-image">
                                        <a href="#">
                                            <img src="img/new/pr_04.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="showcase-text">
                                        <a href="#">
                                            <div class="vendor">valentino</div>
                                            <h4>ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ</h4>
                                            <div class="price">€ 3 120,00</div>
                                        </a>
                                    </div>
                                    <?php include('inc/heart.inc.php') ?>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>