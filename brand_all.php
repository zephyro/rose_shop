<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/topnav.inc.php') ?>
            <!-- -->


            <section class="main">
                <div class="container">
                    <div class="row">

                        <!-- Filter -->
                        <div class="sidebar">

                            <div class="sidenav hidden-xs hidden-sm">

                                <ul>
                                    <li class="active"><a href="#">ВСЕ БРЕНДЫ</a></li>
                                    <li><a href="#">НОВИНКИ</a></li>
                                    <li><a href="#">ОДЕЖДА</a></li>
                                    <li><a href="#">ОБУВЬ</a></li>
                                    <li><a href="#">СУМКИ</a></li>
                                    <li><a href="#">АКСЕССУАРЫ</a></li>
                                    <li><a href="#">БЕЛЬЕ</a></li>
                                    <li><a href="#">КРАСОТА</a></li>
                                    <li><a href="#">ПОДАРКИ</a></li>
                                    <li><a href="#">ДЕТСКОЕ</a></li>
                                </ul>

                            </div>

                        </div>
                        <!-- -->

                        <!-- Content -->
                        <div class="content">


                            <h1 class="text-uppercase">Бренды</h1>

                            <div class="brand-search">
                                <form class="form">
                                    <input type="text" class="form-control" placeholder="поиск бренда">
                                </form>
                            </div>

                            <div class="brand-row">

                                <div class="brand-col">

                                    <div class="brand-item">
                                        <div class="brand-letter">A</div>
                                        <ul>
                                            <li><a href="#">AIIM</a></li>
                                            <li><a href="#">Aquazzura</a></li>
                                            <li><a href="#">ARMANI JEANS</a></li>
                                        </ul>
                                    </div>

                                    <div class="brand-item">
                                        <div class="brand-letter">B</div>
                                        <ul>
                                            <li><a href="#">Brian Dales</a></li>
                                            <li><a href="#">BRUNELLO CUCINELLI</a></li>
                                        </ul>
                                    </div>

                                    <div class="brand-item">
                                        <div class="brand-letter">C</div>
                                        <ul>
                                            <li><a href="#">Citizens of Humanity</a></li>
                                            <li><a href="#">CORNELIANI</a></li>
                                            <li><a href="#">CORTIGIANI</a></li>
                                        </ul>
                                    </div>

                                    <div class="brand-item">
                                        <div class="brand-letter">D</div>
                                        <ul>
                                            <li><a href="#">DIRK BIKKEMBERGS</a></li>
                                            <li><a href="#">Dolce & Gabbana</a></li>
                                            <li><a href="#">Doucal's</a></li>
                                        </ul>
                                    </div>

                                    <div class="brand-item">
                                        <div class="brand-letter">E</div>
                                        <ul>
                                            <li><a href="#">EA7</a></li>
                                            <li><a href="#">ELEVENTY</a></li>
                                            <li><a href="#">ELISABETTA FRANCHI</a></li>
                                            <li><a href="#">EMPORIO ARMANI</a></li>
                                            <li><a href="#">ERMANNO SCERVINO</a></li>
                                            <li><a href="#">ESCADA</a></li>
                                        </ul>
                                    </div>

                                </div>

                                <div class="brand-col">

                                    <div class="brand-item">
                                        <div class="brand-letter">G</div>
                                        <ul>
                                            <li><a href="#">GRAVITEIGHT</a></li>
                                            <li><a href="#">GUCCI</a></li>
                                        </ul>
                                    </div>

                                    <div class="brand-item">
                                        <div class="brand-letter">I</div>
                                        <ul>
                                            <li><a href="#">Inna Honour</a></li>
                                        </ul>
                                    </div>

                                    <div class="brand-item">
                                        <div class="brand-letter">L</div>
                                        <ul>
                                            <li><a href="#">LEITMOTIV</a></li>
                                            <li><a href="#">LIU JO</a></li>
                                            <li><a href="#">LORENA ANTONIAZZI</a></li>
                                            <li><a href="#">LOVE MOSCHINO</a></li>
                                        </ul>
                                    </div>

                                    <div class="brand-item">
                                        <div class="brand-letter">M</div>
                                        <ul>
                                            <li><a href="#">MICHAEL KORS</a></li>
                                            <li><a href="#">MONCLER</a></li>
                                            <li><a href="#">Mr&Mrs</a></li>
                                        </ul>
                                    </div>

                                    <div class="brand-item">
                                        <div class="brand-letter">N</div>
                                        <ul>
                                            <li><a href="#">№21</a></li>
                                        </ul>
                                    </div>

                                    <div class="brand-item">
                                        <div class="brand-letter">P</div>
                                        <ul>
                                            <li><a href="#">PESERICO</a></li>
                                            <li><a href="#">PINKO</a></li>
                                            <li><a href="#">PLEIN SPORT</a></li>
                                            <li><a href="#">PREMIATA</a></li>
                                            <li><a href="#">RHEA COSTA</a></li>
                                        </ul>
                                    </div>

                                </div>

                                <div class="brand-col">

                                    <div class="brand-item">
                                        <div class="brand-letter">S</div>
                                        <ul>
                                            <li><a href="#">SAINT LAURENT</a></li>
                                            <li><a href="#">SANTONI</a></li>
                                            <li><a href="#">Sergio Rossi</a></li>
                                            <li><a href="#">STEFANO RICCI</a></li>
                                            <li><a href="#">STELLA McCARTNEY</a></li>
                                        </ul>
                                    </div>

                                    <div class="brand-item">
                                        <div class="brand-letter">T</div>
                                        <ul>
                                            <li><a href="#">TEREKHOV</a></li>
                                            <li><a href="#">TRESOPHIE</a></li>
                                            <li><a href="#">TWIN SET</a></li>
                                        </ul>
                                    </div>

                                    <div class="brand-item">
                                        <div class="brand-letter">U</div>
                                        <ul>
                                            <li><a href="#">UP JEANS</a></li>
                                        </ul>
                                    </div>

                                    <div class="brand-item">
                                        <div class="brand-letter">V</div>
                                        <ul>
                                            <li><a href="#">VALENTINO</a></li>
                                        </ul>
                                    </div>

                                </div>

                            </div>

                        </div>
                        <!-- -->
                    </div>

                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>