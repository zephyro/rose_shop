<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/topnav.inc.php') ?>
            <!-- -->


            <section class="main">
                <div class="container">

                    <div class="heading">
                        <ul class="heading-nav">
                            <li>
                                <span class="heading-nav-current">Корзина</span>
                            </li>
                            <li>
                                <a href="#" class="heading-nav-item">Избранное</a>
                            </li>
                        </ul>
                        <span class="heading-result">279 товаров</span>
                    </div>

                    <div class="row">

                        <!-- Filter -->
                        <div class="sidebar">

                            <div class="filter-group">
                                <span class="filter-close filter-group-toggle"  data-target=".filter-group"></span>

                                <div class="filter">

                                    <span class="filter-toggle"></span>

                                    <div class="filter-body">

                                        <div class="filter-content">
                                            <ul class="filter-content-list">

                                                <li>
                                                    <span class="filter-name">Брюки</span>

                                                    <ul class="filter-list">
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>клеш</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>прямые</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>скинни</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li>
                                                    <span class="filter-name">Верхняя одежда</span>

                                                    <ul class="filter-list">
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>клеш</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>прямые</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>скинни</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li>
                                                    <span class="filter-name">Джинсы</span>

                                                    <ul class="filter-list">
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>клеш</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>прямые</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>скинни</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li>
                                                    <span class="filter-name">Жакеты / Жилеты</span>

                                                    <ul class="filter-list">
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>клеш</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>прямые</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>скинни</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li>
                                                    <span class="filter-name">Комбинезоны</span>

                                                    <ul class="filter-list">
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>клеш</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>прямые</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>скинни</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li>
                                                    <span class="filter-name">Комплекты</span>

                                                    <ul class="filter-list">
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>клеш</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>прямые</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>скинни</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li>
                                                    <span class="filter-name">Платья</span>

                                                    <ul class="filter-list">
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>клеш</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>прямые</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>скинни</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li>
                                                    <span class="filter-name">Рубашки / Блузки</span>

                                                    <ul class="filter-list">
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>клеш</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>прямые</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>скинни</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li>
                                                    <span class="filter-name">Топы</span>

                                                    <ul class="filter-list">
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>клеш</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>прямые</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>скинни</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li>
                                                    <span class="filter-name">Трикотаж</span>

                                                    <ul class="filter-list">
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>клеш</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>прямые</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>скинни</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li>
                                                    <span class="filter-name">Шорты</span>

                                                    <ul class="filter-list">
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>клеш</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>прямые</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>скинни</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li>
                                                    <span class="filter-name">Юбки</span>

                                                    <ul class="filter-list">
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>клеш</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>прямые</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="check1" value="1">
                                                                <span>скинни</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </li>

                                            </ul>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <!-- -->

                        <!-- Content -->
                        <div class="content">

                            <div class="item-rows">

                                <div class="item-col">
                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item-col">
                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_03.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item-col">
                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_04.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item-col">
                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item-col">
                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_03.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item-col">
                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item-col">
                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item-col">
                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_03.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item-col">
                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_04.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item-col">
                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item-col">
                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_04.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item-col">
                                    <div class="goods">
                                        <div class="goods-image">
                                            <img src="img/look/look_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="goods-brand">valentino</div>
                                        <div class="goods-name">ТУФЛИ С ОТКРЫТЫМ НОСКОМ</div>
                                        <div class="goods-price">€ 620,00</div>
                                        <div class="goods-action">
                                            <select class="form-select">
                                                <option value="выбрать размер">выбрать размер</option>
                                                <option value="XS">XS</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                            </select>
                                            <div class="goods-btn">
                                                <button type="submit" class="btn-form">добавить в корзину</button>
                                                <?php include('inc/heart.inc.php') ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- -->
                    </div>


                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>