<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <header class="header-empty">
                <div class="container">
                    <a class="header-logo" href="#">
                        <img src="img/header_logo.png" class="img-responsive" alt="">
                    </a>
                </div>
            </header>
            <!-- -->

            <section class="main">
                <div class="container">
                    <div class="checkout-header">
                        <h1>Оформление заказа</h1>
                        <p>Возникли сложности с оформлением? Служба по работе с клиентами: 8 (800) 100 28 28</p>
                    </div>

                    <ul class="checkout-step">
                        <li class="active"><a href="#"><span>1. Доставка</span></a></li>
                        <li><a href="#"><span>2. Оплата</span></a></li>
                        <li><a href="#"><span>3. Подтверждение</span></a></li>
                    </ul>

                    <div class="row">
                        <article class="content-checkout">

                            <div class="checkout-user">
                                <div class="checkout-title">Адрес доставки</div>
                                <div class="rows">
                                    <div class="col-md-30">
                                        <label class="form-label">Получатель</label>
                                    </div>
                                    <div class="col-md-40">
                                        <div class="form-group">
                                            <div class="form-wrap">
                                                <input type="text" class="form-input" name="" placeholder="Ваше имя*">
                                                <span class="placeholder">Ваше имя*</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-wrap">
                                                <input type="text" class="form-input" name="" placeholder="Телефон*">
                                                <span class="placeholder">Телефон*</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-40">
                                        <div class="form-group">
                                            <div class="form-wrap">
                                                <input type="text" class="form-input" name="" placeholder="Фамилия">
                                                <span class="placeholder">Фамилия</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-wrap">
                                                <input type="text" class="form-input" name="" placeholder="email">
                                                <span class="placeholder">email</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="checkout-title">Адрес доставки</div>

                            <ul class="checkout-setting">
                                <li>
                                    <label>
                                        <input type="radio" name="delivery" checked>
                                        <span>Курьерской службой</span>
                                    </label>
                                    <ul class="delivery-info">
                                        <li>Курьер доставит ваш заказ в указанное место и передаст в руки.</li>
                                        <li>3 -5 дней</li>
                                        <li>600 Р</li>
                                    </ul>

                                    <div class="form-divider"></div>

                                    <div class="rows">
                                        <div class="col-md-30">
                                        </div>
                                        <div class="col-md-40">
                                            <div class="form-group">
                                                <div class="form-wrap">
                                                    <input type="text" class="form-input" name="" placeholder="Улица*">
                                                    <span class="placeholder">Улица</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="rows">
                                                    <div class="col-xs-50">
                                                        <div class="form-wrap">
                                                            <input type="text" class="form-input" name="" placeholder="Дом*">
                                                            <span class="placeholder">Дом*</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-50">
                                                        <div class="form-wrap">
                                                            <input type="text" class="form-input" name="" placeholder="Квартира*">
                                                            <span class="placeholder">Квартира*</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-40">
                                            <div class="form-group">
                                                <div class="form-wrap">
                                                    <input type="text" class="form-input" name="" placeholder="Индекс*">
                                                    <span class="placeholder">Индекс*</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="delivery">
                                        <span>Доставка до пункта выдачи</span>
                                    </label>
                                    <ul class="delivery-info">
                                        <li>
                                            <p>Вы можете забрать ваш заказ в любом удобном для вас пункте выдачи Boxberry</p>
                                            <a href="#">Пункты выдачи в вашем городе</a>
                                        </li>
                                        <li>2 - 3 дня</li>
                                        <li>бесплатно</li>
                                    </ul>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="delivery">
                                        <span>EMS почта России</span>
                                    </label>
                                    <ul class="delivery-info">
                                        <li>Вы можете забрать ваш заказ в любом удобном для вас пункте выдачи Boxberry</li>
                                        <li>2 - 9 дней</li>
                                        <li>бесплатно</li>
                                    </ul>
                                </li>
                            </ul>

                            <ul class="button-block">
                               <li>
                                   <a href="#" class="btn btn-black-invert">назад</a>
                               </li>
                                <li>
                                    <button type="submit" class="btn btn-black">далее</button>
                                </li>
                            </ul>

                        </article>

                        <aside class="aside">
                            <div class="side-title">Заказ</div>
                            <div class="side-box gray-box">

                                <ul class="side-order">
                                    <li>
                                        <div class="side-order-image">
                                            <img src="img/new/pr_04.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="side-order-text">
                                            <div class="side-order-brand">ISABEL MARANT ETOILE</div>
                                            <div class="side-order-product">ПЛАТЬЕ ИЗ ГИПЮРА И ПАРЧИ</div>
                                            <ul class="side-order-params">
                                                <li>L|48</li>
                                                <li>1</li>
                                                <li><span>29100</span> Р</li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="side-order-image">
                                            <img src="img/new/pr_04.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="side-order-text">
                                            <div class="side-order-brand">ISABEL MARANT ETOILE</div>
                                            <div class="side-order-product">ПЛАТЬЕ ИЗ ГИПЮРА И ПАРЧИ</div>
                                            <ul class="side-order-params">
                                                <li>L|48</li>
                                                <li>1</li>
                                                <li><span>29100</span> Р</li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="side-order-image">
                                            <img src="img/new/pr_04.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="side-order-text">
                                            <div class="side-order-brand">ISABEL MARANT ETOILE</div>
                                            <div class="side-order-product">ПЛАТЬЕ ИЗ ГИПЮРА И ПАРЧИ</div>
                                            <ul class="side-order-params">
                                                <li>L|48</li>
                                                <li>1</li>
                                                <li><span>29100</span> Р</li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>

                                <a href="#" class="btn btn-border">Показать весь заказ</a>

                                <table class="side-summary">
                                    <tr>
                                        <td>Сумма:</td>
                                        <td><span>93 700</span> Р</td>
                                    </tr>
                                    <tr>
                                        <td>Скидка по промокоду:</td>
                                        <td><span>- 3 700</span> Р</td>
                                    </tr>
                                    <tr>
                                        <td>Доставка:</td>
                                        <td><span>600</span> Р</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Итого:</strong></td>
                                        <td><span>84 200</span> Р</td>
                                    </tr>
                                </table>

                            </div>
                        </aside>
                    </div>

                </div>
            </section>

            <!-- Footer -->
            <div class="footer-line"></div>
            <!-- -->

        </div>

        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>