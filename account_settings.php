<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/topnav.inc.php') ?>
            <!-- -->

            <section class="account">
                <div class="container">

                    <h1>Личный кабинет</h1>

                    <div class="row">

                        <article class="account-content">
                            <div class="account-row">
                                <div class="account-col-md-8">
                                    <h4>Любимые бренды</h4>
                                    <div class="scrollbar-inner">
                                        <ul class="account-brand-list">
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="check1" value="1">
                                                    <span>3.1 Phillip Lim</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label>
                                                    <input type="checkbox" name="check1" value="1">
                                                    <span>Aalto</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label>
                                                    <input type="checkbox" name="check1" value="1">
                                                    <span>Acne Studios</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label>
                                                    <input type="checkbox" name="check1" value="1">
                                                    <span>Alexander Terekhov</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label>
                                                    <input type="checkbox" name="check1" value="1">
                                                    <span>Ann Demeulemeester</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label>
                                                    <input type="checkbox" name="check1" value="1">
                                                    <span>Celine</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label>
                                                    <input type="checkbox" name="check1" value="1">
                                                    <span>Current/Elliott</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label>
                                                    <input type="checkbox" name="check1" value="1">
                                                    <span>Erik Frenken</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label>
                                                    <input type="checkbox" name="check1" value="1">
                                                    <span>Etro</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label>
                                                    <input type="checkbox" name="check1" value="1">
                                                    <span>Forte Couture</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label>
                                                    <input type="checkbox" name="check1" value="1">
                                                    <span>Giamba</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label>
                                                    <input type="checkbox" name="check1" value="1">
                                                    <span>3.1 Phillip Lim</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label>
                                                    <input type="checkbox" name="check1" value="1">
                                                    <span>Aalto</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label>
                                                    <input type="checkbox" name="check1" value="1">
                                                    <span>Acne Studios</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label>
                                                    <input type="checkbox" name="check1" value="1">
                                                    <span>Alexander Terekhov</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label>
                                                    <input type="checkbox" name="check1" value="1">
                                                    <span>Ann Demeulemeester</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label>
                                                    <input type="checkbox" name="check1" value="1">
                                                    <span>Celine</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label>
                                                    <input type="checkbox" name="check1" value="1">
                                                    <span>Current/Elliott</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label>
                                                    <input type="checkbox" name="check1" value="1">
                                                    <span>Erik Frenken</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label>
                                                    <input type="checkbox" name="check1" value="1">
                                                    <span>Etro</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label>
                                                    <input type="checkbox" name="check1" value="1">
                                                    <span>Forte Couture</span>
                                                </label>
                                            </li>

                                            <li>
                                                <label>
                                                    <input type="checkbox" name="check1" value="1">
                                                    <span>Giamba</span>
                                                </label>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                                <div class="account-col-md-4">
                                    <h4>Мои размеры</h4>

                                    <ul class="mySize">
                                        <li><a href="#" class="size-selected">ОБУВЬ <b>37</b> <span class="size-delete"></span></a></li>
                                        <li><a href="#" class="size-selected">Одежда <b>M</b> <span class="size-delete"></span></a></li>
                                    </ul>

                                    <div class="form-group">
                                        <div class="form-wrap">
                                            <select class="form-select" name="product_type">
                                                <option value="">Выберите тип товара</option>
                                                <option value="Обувь">Обувь</option>
                                                <option value="Одежда">Одежда</option>
                                            </select>
                                            <span class="select-placeholder">тип товара</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-wrap">
                                            <select class="form-select" name="product_vendor">
                                                <option value="">выберите страну размера</option>
                                                <option value="Европа">Европа</option>
                                                <option value="США">США</option>
                                                <option value="Россия">Россия</option>
                                            </select>
                                            <span class="select-placeholder">страна-производитель</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-wrap">
                                            <select class="form-select" name="product_size">
                                                <option value="XS">XS</option>
                                                <option value="S">S</option>
                                                <option value="M">M</option>
                                                <option value="L">L</option>
                                                <option value="XL">XL</option>
                                                <option value="XXL">XXL</option>
                                            </select>
                                            <span class="select-placeholder">размер</span>
                                        </div>
                                    </div>
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-black btn-sm">добавить свой размер</button>
                                    </div>
                                </div>
                            </div>
                        </article>

                        <aside class="account-sidebar">
                            <ul class="account-nav">
                                <li><a href="#"><span>Личные данные</span></a></li>
                                <li class="active"><a href="#"><span>Мои адреса</span></a></li>
                                <li><a href="#"><span>Мои покупки</span></a></li>
                                <li><a href="#"><span>Персональные настройки</span></a></li>
                                <li><a href="#"><span>Активация сертификата</span></a></li>
                                <li><a href="#"><span>Выйти</span></a></li>
                            </ul>
                        </aside>

                    </div>
                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>