<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/topnav.inc.php') ?>
            <!-- -->

            <section class="account">
                <div class="container">

                    <h1>Личный кабинет</h1>

                    <div class="row">

                        <article class="account-content">

                            <div class="account-row">
                                <div class="account-col-sm">
                                    <label class="form-label">Персональные данные</label>
                                </div>
                                <div class="account-col-sm">
                                    <div class="form-group">
                                        <div class="form-wrap">
                                            <input type="text" class="form-input" name="" placeholder="EMAIL">
                                            <span class="placeholder">EMAIL</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-wrap">
                                            <input type="text" class="form-input" name="lastName" placeholder="фамилия">
                                            <span class="placeholder">фамилия</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-wrap">
                                            <input type="text" class="form-input" name="firstName" placeholder="Имя">
                                            <span class="placeholder">Имя</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-wrap">
                                            <input type="text" class="form-input" name="secondName" placeholder="Ваше имя*">
                                            <span class="placeholder">ОТЧЕСТВО</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="account-col-sm">
                                    <div class="form-group">
                                        <div class="form-wrap">
                                            <select class="form-select" name="sex">
                                                <option value="женский">женский</option>
                                                <option value="мужской">мужской</option>
                                            </select>
                                            <span class="select-placeholder">пол</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-wrap">
                                            <input type="text" class="form-input" name="birthdate" placeholder="ДАТА РОЖДЕНИЯ">
                                            <span class="placeholder">ДАТА РОЖДЕНИЯ</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-wrap">
                                            <input type="text" class="form-input" name="phone" placeholder="ТЕЛЕФОН">
                                            <span class="placeholder">ТЕЛЕФОН</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-wrap">
                                            <button class="btn-text">добавить телефон</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="account-divider"></div>

                            <div class="account-row">
                                <div class="account-col-sm">
                                    <label class="form-label">Изменить пароль</label>
                                </div>
                                <div class="account-col-sm">
                                    <div class="form-group">
                                        <div class="form-wrap">
                                            <input type="text" class="form-input" name="" placeholder="НОВЫЙ ПАРОЛЬ">
                                            <span class="placeholder">НОВЫЙ ПАРОЛЬ</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-wrap">
                                            <input type="text" class="form-input" name="lastName" placeholder="ПОВТОР ПАРОЛЯ">
                                            <span class="placeholder">ПОВТОР ПАРОЛЯ</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-black btn-sm">сохранить</button>
                                    </div>
                                </div>
                                <div class="account-col-sm">

                                </div>
                            </div>

                        </article>

                        <aside class="account-sidebar">
                            <ul class="account-nav">
                                <li class="active"><a href="#"><span>Личные данные</span></a></li>
                                <li><a href="#"><span>Мои адреса</span></a></li>
                                <li><a href="#"><span>Мои покупки</span></a></li>
                                <li><a href="#"><span>Персональные настройки</span></a></li>
                                <li><a href="#"><span>Активация сертификата</span></a></li>
                                <li><a href="#"><span>Выйти</span></a></li>
                            </ul>
                        </aside>

                    </div>
                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>