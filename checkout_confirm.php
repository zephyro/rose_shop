<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <header class="header-empty">
                <div class="container">
                    <a class="header-logo" href="#">
                        <img src="img/header_logo.png" class="img-responsive" alt="">
                    </a>
                </div>
            </header>
            <!-- -->

            <section class="main">
                <div class="container">
                    <div class="checkout-header">
                        <h1>Оформление заказа</h1>
                        <p>Возникли сложности с оформлением? Служба по работе с клиентами: 8 (800) 100 28 28</p>
                    </div>

                    <ul class="checkout-step">
                        <li><a href="#"><span>1. Доставка</span></a></li>
                        <li><a href="#"><span>2. Оплата</span></a></li>
                        <li class="active"><a href="#"><span>3. Подтверждение</span></a></li>
                    </ul>

                    <div class="row">
                        <article class="content-checkout">

                            <ul class="checkout-summary">
                                <li>
                                    <h4>Адрес</h4>
                                    <ul class="checkout-summary-detail">
                                        <li>Страна:  Россия</li>
                                        <li>Город:  Самара</li>
                                        <li>Адрес:  ул. Карла Маркса</li>
                                        <li>Дом:  26, кв. 65</li>
                                    </ul>
                                    <a href="#" class="checkout-change">Изменить</a>
                                </li>
                                <li>
                                    <h4>Доставка</h4>
                                    <ul class="checkout-summary-detail">
                                        <li>Курьерской службой</li>
                                    </ul>
                                    <a href="#" class="checkout-change">Изменить</a>
                                </li>
                                <li>
                                    <h4>Оплата</h4>
                                    <ul class="checkout-summary-detail">
                                        <li>Банковской картой</li>
                                    </ul>
                                    <a href="#" class="checkout-change">Изменить</a>
                                </li>
                            </ul>

                            <ul class="order">
                                <li>
                                    <div class="order-image">
                                        <img src="img/new/pr_04.jpg" class="img-responsive" alt="">
                                    </div>
                                    <ul class="order-text">
                                        <li class="order-product">
                                            <div class="order-brand">ISABEL MARANT ETOILE</div>
                                            <div class="order-name">ПЛАТЬЕ ИЗ ГИПЮРА И ПАРЧИ</div>
                                        </li>
                                        <li class="order-size">L|48</li>
                                        <li class="order-value">1</li>
                                        <li class="order-price"><span>29 100</span> Р</li>
                                    </ul>
                                </li>
                                <li>
                                    <div class="order-image">
                                        <img src="img/new/pr_04.jpg" class="img-responsive" alt="">
                                    </div>
                                    <ul class="order-text">
                                        <li class="order-product">
                                            <div class="order-brand">ISABEL MARANT ETOILE</div>
                                            <div class="order-name">ПЛАТЬЕ ИЗ ГИПЮРА И ПАРЧИ</div>
                                        </li>
                                        <li class="order-size">L|48</li>
                                        <li class="order-value">1</li>
                                        <li class="order-price"><span>29 100</span> Р</li>
                                    </ul>
                                </li>
                                <li>
                                    <div class="order-image">
                                        <img src="img/new/pr_04.jpg" class="img-responsive" alt="">
                                    </div>
                                    <ul class="order-text">
                                        <li class="order-product">
                                            <div class="order-brand">ISABEL MARANT ETOILE</div>
                                            <div class="order-name">ПЛАТЬЕ ИЗ ГИПЮРА И ПАРЧИ</div>
                                        </li>
                                        <li class="order-size">L|48</li>
                                        <li class="order-value">1</li>
                                        <li class="order-price"><span>29 100</span> Р</li>
                                    </ul>
                                </li>
                            </ul>


                        </article>

                        <aside class="aside">
                            <div class="side-box gray-box">

                                <form class="form">
                                    <div class="side-title">Комментарий к заказу</div>
                                    <div class="form-group">
                                        <textarea class="form-control" placeholder="" rows="4"></textarea>
                                    </div>
                                </form>

                                <table class="side-summary">
                                    <tr>
                                        <td>Сумма:</td>
                                        <td><span>93 700</span> Р</td>
                                    </tr>
                                    <tr>
                                        <td>Скидка по промокоду:</td>
                                        <td><span>- 3 700</span> Р</td>
                                    </tr>
                                    <tr>
                                        <td>Доставка:</td>
                                        <td><span>600</span> Р</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Итого:</strong></td>
                                        <td><span>84 200</span> Р</td>
                                    </tr>
                                </table>

                                <div class="form-check">
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="c1">
                                            <span>Подписаться на новости Roselle</span>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="c2">
                                            <span>Согласен с условиями оферты</span>
                                        </label>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-black">ОФОРМИТЬ ЗАКАЗ</button>

                            </div>
                        </aside>
                    </div>

                </div>
            </section>

            <!-- Footer -->
            <div class="footer-line"></div>
            <!-- -->

        </div>

        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>