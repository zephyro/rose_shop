<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/topnav.inc.php') ?>
            <!-- -->


            <section class="slider">
                <div class="container">
                    <div class="slider-source">
                        <div class="slider-item slider-item-01">
                            <div class="slider-item-content">
                                <div class="slider-heading">ERMANNO SCERVINO F/W 17</div>

                                <a href="#" class="btn btn-invert">перейти к покупкам</a>
                            </div>
                        </div>
                        <div class="slider-item slider-item-02">
                            <div class="slider-item-content">
                                <div class="slider-heading">СВЕЖИЙ И ЯРКИЙ «EA7» УЖЕ В ROSELLE</div>

                                <a href="#" class="btn btn-invert">перейти к покупкам</a>
                            </div>
                        </div>
                        <div class="slider-item slider-item-03">
                            <div class="slider-item-content">
                                <div class="slider-heading">DIRK BIKKEMBERGS F/W 17</div>

                                <a href="#" class="btn btn-invert">перейти к покупкам</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="promo">
                <div class="container">
                    <div class="tiles">
                        <div class="tiles-col">
                            <div class="tile-item item-xl">
                                <img src="img/promo_img_01.jpg" class="img-responsive">
                                <div class="tile-wrap">
                                    <div class="outer">
                                        <div class="inner">
                                            <div class="promo-value">
                                                <img src="img/promo_procent.png" alt="">
                                            </div>
                                            <div class="promo-heading">KINGA  MATHE</div>
                                            <a href="#" class="btn-arrow">перейти к покупкам<i><span></span><svg><use xlink:href="#next"></use></svg></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tiles-col">
                            <div class="tile-item">
                                <img src="img/promo_img_02.gif" class="img-responsive">
                                <div class="tile-wrap">
                                    <div class="outer">
                                        <div class="inner">
                                            <div class="promo-heading">КРАСНАЯ ОСЕНЬ</div>
                                            <a href="#" class="btn-arrow">перейти к покупкам<i><svg><use xlink:href="#next"></use></svg></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tile-item">
                                <img src="img/promo_img_03.jpg" class="img-responsive">
                                <div class="tile-wrap">
                                    <div class="outer">
                                        <div class="inner">
                                            <div class="promo-heading">КЛАССИЧЕСКИЙ ДЕНИМ</div>
                                            <a href="#" class="btn-arrow">перейти к покупкам<i><svg><use xlink:href="#next"></use></svg></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="look-block">
                <div class="container">
                    <h2>Сотни образов для любого события от стилистов Roselle</h2>

                    <div class="look">
                        <div class="look-item">
                            <div class="look-kit">
                                <div class="look-kit-name">valentino</div>
                                <div class="look-kit-image">
                                    <img src="img/look/look_base.jpg" class="img-responsive" alt="">

                                </div>
                                <div class="text-center">
                                    <a href="#" class="btn">купить образ</a>
                                </div>
                            </div>

                            <ul class="look-row">
                                <li>

                                   <div class="look-object">
                                       <div class="look-image">
                                           <img src="img/look/look_04.jpg" class="img-responsive">
                                       </div>
                                       <div class="look-object-title">valentino</div>
                                       <div class="look-object-price">€ 1 490,00</div>
                                   </div>

                                   <div class="look-object-hover-inner look-object-01">
                                       <span class="look-object-close"></span>
                                       <div class="look-object-hover">
                                           <div class="look-image">
                                               <img src="img/look/look_04.jpg" class="img-responsive">
                                           </div>
                                           <div class="look-object-title">valentino</div>
                                           <div class="look-object-type">платье</div>
                                           <div class="look-object-price">€ 1 490,00</div>
                                           <div class="look-object-form">
                                               <select class="form-select">
                                                   <option value="выбрать размер">выбрать размер</option>
                                                   <option value="XS">XS</option>
                                                   <option value="M">M</option>
                                                   <option value="L">L</option>
                                                   <option value="XL">XL</option>
                                               </select>
                                               <div class="form-action">
                                                   <button type="submit" class="btn-form">добавить в корзину</button>
                                                   <?php include('inc/heart.inc.php') ?>
                                               </div>
                                           </div>
                                       </div>
                                   </div>

                                </li>
                                <li>

                                    <div class="look-object">
                                        <div class="look-image">
                                            <img src="img/look/look_02.jpg" class="img-responsive">
                                        </div>
                                        <div class="look-object-title">valentino</div>
                                        <div class="look-object-price">€ 1 490,00</div>
                                    </div>


                                    <div class="look-object-hover-inner look-object-02">
                                        <span class="look-object-close"></span>
                                        <div class="look-object-hover">
                                            <div class="look-image">
                                                <img src="img/look/look_02.jpg" class="img-responsive">
                                            </div>
                                            <div class="look-object-title">valentino</div>
                                            <div class="look-object-type">платье</div>
                                            <div class="look-object-price">€ 1 490,00</div>
                                            <div class="look-object-form">
                                                <select class="form-select">
                                                    <option value="выбрать размер">выбрать размер</option>
                                                    <option value="XS">XS</option>
                                                    <option value="M">M</option>
                                                    <option value="L">L</option>
                                                    <option value="XL">XL</option>
                                                </select>
                                                <div class="form-action">
                                                    <button type="submit" class="btn-form">добавить в корзину</button>
                                                    <?php include('inc/heart.inc.php') ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </li>
                                <li>

                                    <div class="look-object">
                                        <div class="look-image">
                                            <img src="img/look/look_03.jpg" class="img-responsive">
                                        </div>
                                        <div class="look-object-title">valentino</div>
                                        <div class="look-object-price">€ 1 490,00</div>
                                    </div>


                                    <div class="look-object-hover-inner look-object-03">
                                        <span class="look-object-close"></span>
                                        <div class="look-object-hover">
                                            <div class="look-image">
                                                <img src="img/look/look_03.jpg" class="img-responsive">
                                            </div>
                                            <div class="look-object-title">valentino</div>
                                            <div class="look-object-type">платье</div>
                                            <div class="look-object-price">€ 1 490,00</div>
                                            <div class="look-object-form">
                                                <select class="form-select">
                                                    <option value="выбрать размер">выбрать размер</option>
                                                    <option value="XS">XS</option>
                                                    <option value="M">M</option>
                                                    <option value="L">L</option>
                                                    <option value="XL">XL</option>
                                                </select>
                                                <div class="form-action">
                                                    <button type="submit" class="btn-form">добавить в корзину</button>
                                                    <?php include('inc/heart.inc.php') ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </li>
                                <li>

                                    <div class="look-object">
                                        <div class="look-image">
                                            <img src="img/look/look_04.jpg" class="img-responsive">
                                        </div>
                                        <div class="look-object-title">valentino</div>
                                        <div class="look-object-price">€ 1 490,00</div>
                                    </div>


                                    <div class="look-object-hover-inner look-object-04">
                                        <span class="look-object-close"></span>
                                        <div class="look-object-hover">
                                            <div class="look-image">
                                                <img src="img/look/look_04.jpg" class="img-responsive">
                                            </div>
                                            <div class="look-object-title">valentino</div>
                                            <div class="look-object-type">платье</div>
                                            <div class="look-object-price">€ 1 490,00</div>
                                            <div class="look-object-form">
                                                <select class="form-select">
                                                    <option value="выбрать размер">выбрать размер</option>
                                                    <option value="XS">XS</option>
                                                    <option value="M">M</option>
                                                    <option value="L">L</option>
                                                    <option value="XL">XL</option>
                                                </select>
                                                <div class="form-action">
                                                    <button type="submit" class="btn-form">добавить в корзину</button>
                                                    <?php include('inc/heart.inc.php') ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <section class="consultation-block hidden-xs hidden-sm">
                <div class="container">
                    <h2>Получить консультацию стилиста</h2>
                    <p>Наш стилист подберет образ специально для вас</p>
                    <a href="#" class="btn btn-black">получить консультацию стилиста</a>
                </div>
            </section>

            <section class="new-block">
                <div class="container">
                    <h2>Новинки</h2>
                    <div class="new-slide">

                        <div class="new-slide-item">

                            <div class="showcase-item">
                                <div class="showcase-image">
                                    <a href="#">
                                        <img src="img/new/pr_01.jpg" alt="">
                                    </a>
                                </div>
                                <div class="showcase-text">
                                    <a href="#">
                                        <div class="vendor">valentino</div>
                                        <h4>ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ</h4>
                                        <div class="price">€ 3 120,00</div>
                                    </a>
                                </div>
                                <?php include('inc/heart.inc.php') ?>
                            </div>

                        </div>

                        <div class="new-slide-item">

                            <div class="showcase-item">
                                <div class="showcase-image">
                                    <a href="#">
                                        <img src="img/new/pr_02.jpg" alt="">
                                    </a>
                                </div>
                                <div class="showcase-text">
                                    <a href="#">
                                        <div class="vendor">valentino</div>
                                        <h4>ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ</h4>
                                        <div class="price">€ 3 120,00</div>
                                    </a>
                                </div>
                                <?php include('inc/heart.inc.php') ?>
                            </div>

                        </div>

                        <div class="new-slide-item">

                            <div class="showcase-item">
                                <div class="showcase-image">
                                    <a href="#">
                                        <img src="img/new/pr_03.jpg" alt="">
                                    </a>
                                </div>
                                <div class="showcase-text">
                                    <a href="#">
                                        <div class="vendor">valentino</div>
                                        <h4>ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ</h4>
                                        <div class="price">€ 3 120,00</div>
                                    </a>
                                </div>
                                <?php include('inc/heart.inc.php') ?>
                            </div>

                        </div>

                        <div class="new-slide-item">

                            <div class="showcase-item">
                                <div class="showcase-image">
                                    <a href="#">
                                        <img src="img/new/pr_04.jpg" alt="">
                                    </a>
                                </div>
                                <div class="showcase-text">
                                    <a href="#">
                                        <div class="vendor">valentino</div>
                                        <h4>ПЛАТЬЕ ИЗ ХЛОПКОВОЙ РОГОЖКИ</h4>
                                        <div class="price">€ 3 120,00</div>
                                    </a>
                                </div>
                                <?php include('inc/heart.inc.php') ?>
                            </div>

                        </div>

                    </div>
                </div>
            </section>

            <section class="preview-block">
                <div class="container">
                    <ul class="preview">
                        <li>
                            <div class="preview-item">
                                <img src="img/review_01.jpg" class="img-responsive">
                                <div class="preview-wrap">
                                    <div class="outer">
                                        <div class="inner">
                                            <div class="preview-date">17 МАЯ 2017</div>
                                            <div class="preview-heading"><a href="#">Принципы дизайна</a></div>
                                            <p>Часть 1</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="preview-subscribe hidden-xs hidden-sm">
                                <div class="subscribe-text">
                                    <div class="preview-heading">Готовятся к релизу</div>
                                    <p>Принципы дизайна. Часть 2</p>
                                    <form class="form">
                                        <ul class="preview-form">
                                            <li>
                                                <input type="text" class="preview-input" placeholder="email">
                                            </li>
                                            <li>
                                                <button type="submit" class="btn-border">узнать первым</button>
                                            </li>

                                        </ul>
                                    </form>
                                </div>
                            </div>
                        </li>

                        <li>
                            <ul class="preview-items">
                                <li>
                                    <img src="img/review_02.jpg" class="img-responsive">
                                    <div class="preview-box">
                                        <div class="preview-date">17 МАЯ 2017</div>
                                        <div class="preview-heading"><a href="#">Этой весной сафари — самая романтичная тенденция сезона</a></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="preview-img-inner">
                                        <img src="img/review_03.jpg" class="img-responsive">
                                    </div>
                                    <div class="preview-box">
                                        <div class="preview-date">17 МАЯ 2017</div>
                                        <div class="preview-heading"><a href="#">Coachella в летнем гардеробе</a></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="preview-img-inner">
                                        <img src="img/review_04.jpg" class="img-responsive">
                                    </div>
                                    <div class="preview-box">
                                        <div class="preview-date">17 МАЯ 2017</div>
                                        <div class="preview-heading"><a href="#">Самые модные «двойки» сезона</a></div>
                                    </div>
                                </li>
                            </ul>
                        </li>

                        <div class="preview-subscribe hidden-md hidden-lg">
                            <div class="subscribe-text">
                                <div class="preview-heading">Готовятся к релизу</div>
                                <p>Принципы дизайна. Часть 2</p>
                                <form class="form">
                                    <ul class="preview-form">
                                        <li>
                                            <input type="text" class="preview-input" placeholder="email">
                                        </li>
                                        <li>
                                            <button type="submit" class="btn-border">узнать первым</button>
                                        </li>

                                    </ul>
                                </form>
                            </div>
                        </div>
                    </ul>
                </div>
            </section>

            <section class="vendor-block">
                <div class="container">
                    <ul class="vendors">
                        <li><img src="svg/vendors/logo_AJ_roselle_bl.svg" alt=""></li>
                        <li><img src="svg/vendors/logo_bikke_roselle_bl.svg" alt=""></li>
                        <li><img src="svg/vendors/logo_brunello_roselle_bl.svg" alt=""></li>
                        <li><img src="svg/vendors/logo_corneliani_roselle_bl.svg" alt=""></li>
                        <li><img src="svg/vendors/logo_cortigiani_roselle_bl.svg" alt=""></li>
                        <li><img src="svg/vendors/logo_dolce_roselle_bl.svg" alt=""></li>

                        <li><img src="svg/vendors/logo_EA_roselle_bl.svg" alt=""></li>
                        <li><img src="svg/vendors/logo_EA7_roselle_bl.svg" alt=""></li>
                        <li><img src="svg/vendors/logo_EF_roselle_bl.svg" alt=""></li>
                        <li><img src="svg/vendors/logo_escada_roselle_bl.svg" alt=""></li>
                        <li><img src="svg/vendors/logo_gucci_roselle_bl.svg" alt=""></li>
                        <li><img src="svg/vendors/logo_KORS_roselle_bl.svg" alt=""></li>

                        <li><img src="svg/vendors/logo_liu jo_roselle_bl.svg" alt=""></li>
                        <li><img src="svg/vendors/logo_LM_roselle_bl.svg" alt=""></li>
                        <li><img src="svg/vendors/logo_moncler_roselle_bl.svg" alt=""></li>
                        <li><img src="svg/vendors/logo_pinko_roselle_bl.svg" alt=""></li>
                        <li><img src="svg/vendors/logo_premiata_roselle_bl.svg" alt=""></li>
                        <li><img src="svg/vendors/logo_saint_laurent_roselle_bl.svg" alt=""></li>

                        <li><img src="svg/vendors/logo_santoni_roselle_bl.svg" alt=""></li>
                        <li><img src="svg/vendors/logo_sergio_roselle_bl.svg" alt=""></li>
                        <li><img src="svg/vendors/logo_stefano_ricci_roselle_bl.svg" alt=""></li>
                        <li><img src="svg/vendors/logo_stella_roselle_bl.svg" alt=""></li>
                        <li><img src="svg/vendors/logo_twin_roselle_bl.svg" alt=""></li>
                        <li><img src="svg/vendors/logo_valentino_roselle_bl.svg" alt=""></li>

                    </ul>
                    <div class="text-center">
                        <a class="brand-view">
                            <span class="open-text">показатьвсе бренды</span>
                            <span class="close-text">свернуть</span>
                        </a>
                    </div>
                </div>
            </section>

            <section class="advantage-block">
                <div class="container">
                    <h2>Преимущества Roselle</h2>
                    <ul class="advantage clearfix">
                        <li>Лучшие мировые бренды онлайн</li>
                        <li>Быстрая доставка по России и СНГ</li>
                        <li>Бесплатные консультации профессиональных стилистов</li>
                        <li>ROSELLE - о главных тенденциях мужской моды</li>
                    </ul>
                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>
