<!doctype html>
<html class="no-js" lang="ru">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    
    <body>

        <div class="page">

            <!-- Header -->
            <header class="header-empty">
                <div class="container">
                    <a class="header-logo" href="#">
                        <img src="img/header_logo.png" class="img-responsive" alt="">
                    </a>
                </div>
            </header>
            <!-- -->

            <section class="main">
                <div class="container">
                    <div class="checkout-header">
                        <h1>Оформление заказа</h1>
                        <p>Возникли сложности с оформлением? Служба по работе с клиентами: 8 (800) 100 28 28</p>
                    </div>

                    <ul class="checkout-step">
                        <li><a href="#"><span>1. Доставка</span></a></li>
                        <li class="active"><a href="#"><span>2. Оплата</span></a></li>
                        <li><a href="#"><span>3. Подтверждение</span></a></li>
                    </ul>

                    <div class="row">
                        <article class="content-checkout">

                            <ul class="checkout-setting">
                                <li>
                                    <label>
                                        <input type="radio" name="delivery" checked>
                                        <span>Оплата банковской картой</span>
                                    </label>
                                    <span>5% скидка при этом способе оплаты</span>
                                    <ul class="payment">
                                        <li class="p-ipay">
                                            <img src="svg/ipay.svg" alt="" class="img-responsive">
                                        </li>
                                        <li class="p-visa">
                                            <img src="svg/visa.svg" alt="" class="img-responsive">
                                        </li>
                                        <li class="p-ms">
                                            <img src="svg/mastercarg.svg" alt="" class="img-responsive">
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="delivery">
                                        <span>Оплата PayPal</span>
                                    </label>
                                    <span>5% скидка при этом способе оплаты</span>
                                    <ul class="payment">
                                        <li class="p-paypal">
                                            <img src="svg/paypal.svg" alt="" class="img-responsive">
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="delivery">
                                        <span>Наличными при получении</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="delivery">
                                        <span>Оплата банковской картой при получении</span>
                                    </label>
                                    <span>5% скидка при этом способе оплаты</span>
                                    <ul class="payment">
                                        <li class="p-visa">
                                            <img src="svg/visa.svg" alt="" class="img-responsive">
                                        </li>
                                        <li class="p-paypal">
                                            <img src="svg/paypal.svg" alt="" class="img-responsive">
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                            <ul class="button-block">
                               <li>
                                   <a href="#" class="btn btn-black-invert">назад</a>
                               </li>
                                <li>
                                    <button type="submit" class="btn btn-black">далее</button>
                                </li>
                            </ul>

                        </article>

                        <aside class="aside">
                            <div class="side-title">Заказ</div>
                            <div class="side-box gray-box">

                                <ul class="side-order">
                                    <li>
                                        <div class="side-order-image">
                                            <img src="img/new/pr_04.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="side-order-text">
                                            <div class="side-order-brand">ISABEL MARANT ETOILE</div>
                                            <div class="side-order-product">ПЛАТЬЕ ИЗ ГИПЮРА И ПАРЧИ</div>
                                            <ul class="side-order-params">
                                                <li>L|48</li>
                                                <li>1</li>
                                                <li><span>29100</span> Р</li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="side-order-image">
                                            <img src="img/new/pr_04.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="side-order-text">
                                            <div class="side-order-brand">ISABEL MARANT ETOILE</div>
                                            <div class="side-order-product">ПЛАТЬЕ ИЗ ГИПЮРА И ПАРЧИ</div>
                                            <ul class="side-order-params">
                                                <li>L|48</li>
                                                <li>1</li>
                                                <li><span>29100</span> Р</li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="side-order-image">
                                            <img src="img/new/pr_04.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="side-order-text">
                                            <div class="side-order-brand">ISABEL MARANT ETOILE</div>
                                            <div class="side-order-product">ПЛАТЬЕ ИЗ ГИПЮРА И ПАРЧИ</div>
                                            <ul class="side-order-params">
                                                <li>L|48</li>
                                                <li>1</li>
                                                <li><span>29100</span> Р</li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>

                                <a href="#" class="btn btn-border">Показать весь заказ</a>

                                <table class="side-summary">
                                    <tr>
                                        <td>Сумма:</td>
                                        <td><span>93 700</span> Р</td>
                                    </tr>
                                    <tr>
                                        <td>Скидка по промокоду:</td>
                                        <td><span>- 3 700</span> Р</td>
                                    </tr>
                                    <tr>
                                        <td>Доставка:</td>
                                        <td><span>600</span> Р</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Итого:</strong></td>
                                        <td><span>84 200</span> Р</td>
                                    </tr>
                                </table>

                            </div>
                        </aside>
                    </div>

                </div>
            </section>

            <!-- Footer -->
            <div class="footer-line"></div>
            <!-- -->

        </div>

        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>